﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for FramingAssistantView.xaml
    /// </summary>
    public partial class FramingAssistantView : UserControl {

        public FramingAssistantView() {
            InitializeComponent();
        }
    }
}