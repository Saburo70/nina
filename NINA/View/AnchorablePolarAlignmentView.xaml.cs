﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorablePolarAlignmentView.xaml
    /// </summary>
    public partial class AnchorablePolarAlignmentView : UserControl {

        public AnchorablePolarAlignmentView() {
            InitializeComponent();
        }
    }
}